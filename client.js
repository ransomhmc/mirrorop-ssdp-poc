var Client = require('node-ssdp').Client
  , client = new Client()

function parseHostNameAndIP(headers) {
	var tokens = headers.LOCATION.split('/')
	console.log('Found MirrorOp device: Hostname='+tokens[1]+',IP='+tokens[0])
}

client.on('notify', function () {
  console.log('Got a notification.')
})

client.on('response', function inResponse(headers, code, rinfo) {
  //console.log('Got a response to an m-search:\n%d\n%s\n%s', code, JSON.stringify(headers, null, '  '), JSON.stringify(rinfo, null, '  '))
	if (headers.USN.includes('mirrorop2') == false)
		return
	var json_header = JSON.stringify(headers)
	var json_rinfo = JSON.stringify(rinfo)

	parseHostNameAndIP(headers)
	//console.log('header:\n'+json_header)
	//console.log('rinfo:\n'+json_rinfo)
})

//client.search('urn:schemas-upnp-org:service:ContentDirectory:1')
//client.search('urn:schemas-upnp-org:service:Mirrorop2:1')
//client.search('urn:schemas-upnp-org:service:mirrorop2:1')
//client.search('urn:schemas-upnp-org:device:MediaServer:1')
//client.search('upnp:rootdevice')
client.search('ssdp:all')

setTimeout(function () {
  client.stop()
}, 10000)
