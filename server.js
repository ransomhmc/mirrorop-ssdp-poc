var Server = require('node-ssdp').Server
      , server = new Server({
	location: require('ip').address() + '/' + require('os').hostname(),
	sourcePort: 1900}) ;

server.addUSN('upnp:rootdevice')
/*
server.addUSN('urn:schemas-upnp-org:device:MediaServer:1')
server.addUSN('urn:schemas-upnp-org:service:ContentDirectory:1')
server.addUSN('urn:schemas-upnp-org:service:ConnectionManager:1')
*/
server.addUSN('urn:schemas-upnp-org:service:mirrorop2:1')
server.on('advertise-alive', function (heads) {
  //console.log('advertise-alive', heads)
})

server.on('advertise-bye', function (heads) {
  //console.log('advertise-bye', heads)
})

server.start()
