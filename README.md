* curry:
  * wget https://git.barco.com/rest/archive/latest/projects/~RAHU/repos/mirrorop-ssdp-poc/archive?format=zip
  * untar, run curry-setup.sh
  * logout & login
  * npm install
  * node server.js
* win/mac: 
  * install nodejs from https://nodejs.org/en/download/current/
  * git clone https://RAHU@git.barco.com/scm/~rahu/mirrorop-ssdp-poc.git
  * npm install
  * node client.js