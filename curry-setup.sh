#!/bin/sh
INSTALL_DIR=/mnt/mmcblk1p8/ssdp

mkdir -p $INSTALL_DIR
cd $INSTALL_DIR
rm -rf node-v8.9.1-linux-armv7l.tar.gz
rm -rf node-v8.9.1-linux-armv7l
wget https://nodejs.org/dist/v8.9.1/node-v8.9.1-linux-armv7l.tar.gz
tar zxf node-v8.9.1-linux-armv7l.tar.gz
cd node-v8.9.1-linux-armv7l/bin
echo export PATH=$PATH:`pwd` >> /etc/profile
sync
echo 'please re-login to enable nodejs from shell'